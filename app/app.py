from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def fly():
    return render_template("pen_hallo.html")

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
